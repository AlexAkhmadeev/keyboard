export declare function deferred(): {
    resolve: () => any;
    promise: Promise<void>;
};
export declare function printCallbackRemoveWarning(key?: string): void;
