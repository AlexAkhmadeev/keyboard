export function deferred() {
    let resolveCallback;

    const promise = new Promise<void>((resolve) => {
        resolveCallback = resolve;
    });

    return {
        resolve: () => resolveCallback(),
        promise,
    };
}

export function printCallbackRemoveWarning(key?: string): void {
    if (key) {
        console.warn(`There is no such callback on key ${key} you are trying to remove`);
    } else {
        console.warn('There is no such callback on document you are trying to remove');
    }
}