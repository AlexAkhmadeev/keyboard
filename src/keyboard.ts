import {Key} from './keys';
import {deferred, printCallbackRemoveWarning} from './utils';
import bind from 'bind-decorator';

class Keyboard {
    private readonly keys: Set<string>;
    private callbacks: ICallbacksData;
    private running: boolean;
    private readonly longPressTimers: ILongPressTimers;
    private readonly waitKeysDfd: IWaitKeys;
    private config: IConfig;

    constructor() {
        this.keys = new Set<string>();

        this.callbacks = {
            keydown: {},
            keypress: {},
            keyup: {},
            longpress: {},
            commonKeydown: [],
            commonKeypress: [],
            commonKeyup: [],
            commonLongpress: [],
            onChange: [],
        }

        this.config = {
            longPressDelayMs: 800,
        }

        this.longPressTimers = {};
        this.waitKeysDfd = {};

        this.initialize();
    }

    @bind
    public getKeys(): Record<string, true> {
        return Array.from(this.keys).reduce<Record<string, true>>((acc, value) => {
            acc[value] = true;
            return acc;
        }, {});
    }

    @bind
    public onChangeKeys(callback: (keys: Record<string, true>) => void) {
        this.callbacks.onChange.push(callback);
    }

    @bind
    public configure(options: IConfig): void {
        this.config = {
            ...this.config,
            ...options,
        }
    }

    @bind
    public getPressedKeysArray(): Array<string> {
        return Array.from(this.keys);
    }

    @bind
    onKeyDown(callback: Function): void {
        this.callbacks.commonKeydown.push(callback);
    }

    @bind
    offKeyDown(callback: Function): void {
        const index = this.callbacks.commonKeydown.indexOf(callback);
        if (index !== -1) {
            this.callbacks.commonKeydown.splice(index, 1);
        } else {
            printCallbackRemoveWarning();
        }
    }

    @bind
    onKeyPress(callback: Function): void {
        this.callbacks.commonKeypress.push(callback);
    }

    @bind
    offKeyPress(callback: Function): void {
        const index = this.callbacks.commonKeypress.indexOf(callback);
        if (index !== -1) {
            this.callbacks.commonKeypress.splice(index, 1);
        } else {
            printCallbackRemoveWarning();
        }
    }

    @bind
    onKeyUp(callback: Function): void {
        this.callbacks.commonKeyup.push(callback);
    }

    @bind
    offKeyUp(callback: Function): void {
        const index = this.callbacks.commonKeyup.indexOf(callback);
        if (index !== -1) {
            this.callbacks.commonKeyup.splice(index, 1);
        } else {
            printCallbackRemoveWarning();
        }
    }

    @bind
    onAnyLongPress(callback: Function): void {
        this.callbacks.commonLongpress.push(callback);
    }

    @bind
    offAnyLongPress(callback: Function): void {
        const index = this.callbacks.commonLongpress.indexOf(callback);
        if (index !== -1) {
            this.callbacks.commonLongpress.splice(index, 1);
        } else {
            printCallbackRemoveWarning();
        }
    }

    @bind
    public onDown(key: Key, callback: Function): void {
        if (Array.isArray(this.callbacks.keydown[key])) {
            this.callbacks.keydown[key].push(callback);
        } else {
            this.callbacks.keydown[key] = [callback];
        }
    }

    @bind
    public offDown(key: Key, callback: Function) {
        if (!Array.isArray(this.callbacks.keydown[key])) {
            printCallbackRemoveWarning(key);
            return;
        }

        const index = this.callbacks.keydown[key].indexOf(callback);

        if (index === -1) {
            printCallbackRemoveWarning(key);
            return;
        }

        this.callbacks.keydown[key].splice(index, 1);
    }

    @bind
    public onPress(key: Key, callback: Function): void {
        if (Array.isArray(this.callbacks.keypress[key])) {
            this.callbacks.keypress[key].push(callback);
        } else {
            this.callbacks.keypress[key] = [callback];
        }
    }

    @bind
    public offPress(key: Key, callback: Function) {
        if (!Array.isArray(this.callbacks.keypress[key])) {
            printCallbackRemoveWarning(key);
            return;
        }

        const index = this.callbacks.keypress[key].indexOf(callback);

        if (index === -1) {
            printCallbackRemoveWarning(key);
            return;
        }

        this.callbacks.keypress[key].splice(index, 1);
    }

    @bind
    public onUp(key: Key, callback: Function): void {
        if (Array.isArray(this.callbacks.keyup[key])) {
            this.callbacks.keyup[key].push(callback);
        } else {
            this.callbacks.keyup[key] = [callback];
        }
    }

    @bind
    public offUp(key: Key, callback: Function) {
        if (!Array.isArray(this.callbacks.keyup[key])) {
            printCallbackRemoveWarning(key);
            return;
        }

        const index = this.callbacks.keyup[key].indexOf(callback);

        if (index === -1) {
            printCallbackRemoveWarning(key);
            return;
        }

        this.callbacks.keyup[key].splice(index, 1);
    }

    @bind
    public onLongPress(key: Key, callback: Function): void {
        if (Array.isArray(this.callbacks.longpress[key])) {
            this.callbacks.longpress[key].push(callback);
        } else {
            this.callbacks.longpress[key] = [callback];
        }
    }

    @bind
    public offLongPress(key: Key, callback: Function): void {
        if (!Array.isArray(this.callbacks.longpress[key])) {
            printCallbackRemoveWarning(key);
            return;
        }

        const index = this.callbacks.longpress[key].indexOf(callback);

        if (index === -1) {
            printCallbackRemoveWarning(key);
            return;
        }

        this.callbacks.longpress[key].splice(index, 1);
    }

    @bind
    public isPressed(key: Key): boolean {
        return this.keys.has(key);
    }

    @bind
    public isLeftControlPressed(): boolean {
        return this.keys.has(Key.ControlLeft);
    }

    @bind
    public isRightControlPressed(): boolean {
        return this.keys.has(Key.ControlRight);
    }

    @bind
    public isControlPressed(): boolean {
        return this.isLeftControlPressed() || this.isRightControlPressed();
    }

    @bind
    public isLeftShiftPressed(): boolean {
        return this.keys.has(Key.ShiftLeft);
    }

    @bind
    public isRightShiftPressed(): boolean {
        return this.keys.has(Key.ShiftRight);
    }

    @bind
    public isShiftPressed(): boolean {
        return this.isLeftShiftPressed() || this.isRightShiftPressed();
    }

    @bind
    public isLeftAltPressed(): boolean {
        return this.keys.has(Key.AltLeft);
    }

    @bind
    public isRightAltPressed(): boolean {
        return this.keys.has(Key.AltRight);
    }

    @bind
    public isAltPressed(): boolean {
        return this.isLeftAltPressed() || this.isRightAltPressed();
    }

    @bind
    public isLeftMetaPressed(): boolean {
        return this.keys.has(Key.MetaLeft);
    }

    @bind
    public isRightMetaPressed(): boolean {
        return this.keys.has(Key.MetaLeft);
    }

    @bind
    public isMetaPressed(): boolean {
        return this.isLeftMetaPressed() || this.isRightMetaPressed();
    }

    @bind
    public isTabPressed(): boolean {
        return this.keys.has(Key.Tab);
    }

    @bind
    public async waitKey(key: Key): Promise<void> {
        this.waitKeysDfd[key] = deferred();
        return this.waitKeysDfd[key].promise;
    }

    @bind
    public pause(): void {
        this.removeListeners();
        this.running = false;
    }

    @bind
    public resume(): void {
        if (this.running) {
            console.warn('Keyboard already running');
            return;
        }

        this.addListeners();
        this.running = true;
    }

    @bind
    private initialize(): void {
        this.addListeners();
    }

    @bind
    private addListeners(): void {
        window.addEventListener('keydown', this.handleKeyDown);
        window.addEventListener('keypress', this.handleKeyPress);
        window.addEventListener('keyup', this.handleKeyUp);
        window.addEventListener('blur', this.clearKeys);
    }

    @bind
    private removeListeners(): void {
        window.removeEventListener('keydown', this.handleKeyDown);
        window.removeEventListener('keypress', this.handleKeyPress);
        window.removeEventListener('keyup', this.handleKeyUp);
        window.removeEventListener('blur', this.clearKeys);
    }

    @bind
    private handleKeyDown(e: KeyboardEvent): void {
        this.keys.add(e.code);
        this.emitOnChangeCallbacks();

        const toCallCallbacks = this.callbacks.keydown[e.code];

        if (Array.isArray(toCallCallbacks)) {
            for (const callback of toCallCallbacks) {
                callback(e);
            }
        }

        for (const callback of this.callbacks.commonKeydown) {
            callback(e);
        }

        if (this.waitKeysDfd[e.code]) {
            this.waitKeysDfd[e.code].resolve();
            delete this.waitKeysDfd[e.code];
        }

        if (this.longPressTimers[e.code]) {
            return;
        }

        this.longPressTimers[e.code] = setTimeout(() => {
            if (this.keys.has(e.code)) {
                this.handleLongPress(e);
            }
        }, this.config.longPressDelayMs);
    }

    @bind
    private handleKeyPress(e: KeyboardEvent): void {
        const toCallCallbacks = this.callbacks.keypress[e.code];

        if (Array.isArray(toCallCallbacks)) {
            for (const callback of toCallCallbacks) {
                callback(e);
            }
        }

        for (const callback of this.callbacks.commonKeypress) {
            callback(e);
        }
    }

    @bind
    private handleKeyUp(e: KeyboardEvent): void {
        this.keys.delete(e.code);
        this.emitOnChangeCallbacks();

        const toCallCallbacks = this.callbacks.keyup[e.code];

        if (Array.isArray(toCallCallbacks)) {
            for (const callback of toCallCallbacks) {
                callback(e);
            }
        }

        for (const callback of this.callbacks.commonKeyup) {
            callback(e);
        }

        clearInterval(this.longPressTimers[e.code]);
        delete this.longPressTimers[e.code];
    }

    @bind
    private handleLongPress(e: KeyboardEvent): void {
        const toCallCallbacks = this.callbacks.longpress[e.code];

        if (Array.isArray(toCallCallbacks)) {
            for (const callback of toCallCallbacks) {
                callback(e);
            }
        }

        for (const callback of this.callbacks.commonLongpress) {
            callback(e);
        }
    }

    @bind
    private clearKeys() {
        this.keys.clear();
        this.emitOnChangeCallbacks();
    }

    private emitOnChangeCallbacks() {
        for (const callback of this.callbacks.onChange) {
            callback(this.getKeys());
        }
    }
}

interface ICallbacksData {
    keydown: {[key: string]: Array<Function>};
    keyup: {[key: string]: Array<Function>};
    keypress: {[key: string]: Array<Function>};
    longpress: {[key: string]: Array<Function>};
    commonKeydown: Array<Function>;
    commonKeyup: Array<Function>;
    commonKeypress: Array<Function>;
    commonLongpress: Array<Function>;
    onChange: Array<Function>;
}

interface IConfig {
    longPressDelayMs: number;
}

interface ILongPressTimers {
    [key: string]: NodeJS.Timeout;
}

interface IWaitKeys {
    [key: string]: ReturnType<typeof deferred>;
}

export const keyboard = new Keyboard();
